#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>
#include "header_extrp.h"

#define max_iter 100
#define epsabs 0.0000000001
#define epsrel 0.0
#define dz 0.01
//to compile use: $g++ cp.c functions_extrp.c -lgsl -o cp


int find_deriv_zero(double x_lo, double x_hi, gsl_root_fsolver * s, gsl_function &F,db_params * params, int &status, double &r);

double  find_glob_minim(db_params &params,gsl_root_fsolver *s, gsl_function F, double zpt);

//Function to find the closest point to the dumbbell
int main (void)
{
double z;
double zpt,xpt,ypt;
double r1,r2;
double zcp,rcp,xcp,ycp;
//for convergence test:
//|x_lo - x_hi| < epsabs + epsrel min(|x_lo|,|x_hi|)
double x_lo=0.7, x_hi=0.8;
double r=0;
int inside,tot_inside;
int i,j,k;
db_params params={acs,bcs,ccs,dcs,ecs,fcs,0,0}; 

//Initialize function
gsl_function F;
F.function = &db_sq_dist_deriv;

//Initialize solver:
//declare pointer to "solver type" class ;
const gsl_root_fsolver_type * T;
//creates instance of Brent's method solver
T = gsl_root_fsolver_brent;
//Declare pointer to state structure;
gsl_root_fsolver *s;
//Create instance of state for T solver;
s = gsl_root_fsolver_alloc(T);

FILE * file_cp;

if((file_cp=fopen("./data3d/cp_data.dat","w"))==NULL){printf("cannot open cp_data file\n");exit(0);}

fprintf(file_cp,"%d\n",m1*m2*m3);
tot_inside=0;


//zpt=0.02;
//params.rpt=i*0.02;


for(k=0; k<m3; k++){
	for(i=0;i<m1;i++){
		for(j=0;j<m2;j++){
			zpt=-L3+k*dx;
			ypt=-L2+j*dx;
			xpt=-L1+i*dx;
			params.rpt=sqrt(xpt*xpt+ypt*ypt);
			//the function assigns the value to params.zpt
			zcp=find_glob_minim(params, s, F, zpt);
			//determine if the point lies inside the dumbbell
			if(fabs(zpt)<zb){
				if(params.rpt< db_rad(zpt,&params)){
					inside=1;
					tot_inside++;
				}
				else inside=0;
			}
			else inside=0;
			
			//find xcp and ycp
			//the radii of cp and pt are colinear in the xy plane
			rcp=db_rad(zcp,&params);
			if(params.rpt==0){printf("params.rpt==0 in cp");exit(0); }
			xcp=rcp*xpt/params.rpt;
			ycp=rcp*ypt/params.rpt;
	
			fprintf(file_cp,"%.10f %.10f %.10f %.10f %d\n",xcp,ycp,zcp,db_dist(zcp,&params),inside);
				
		}//xpt
	}//ypt
}//zpt
//printf("%f %f %f\n",params.rpt,rcp,zcp);
printf("total points %d total inside points %d dx %f %d %d %d \n",m1*m2*m3,tot_inside,dx,m1,m2,m3);

fclose(file_cp);

/*
FILE * test;
if((test=fopen("./testcp.dat","w"))==NULL){printf("cannot open test\n");exit(0);}
params.rpt=0.01;
zpt=-1.2;
while(zpt<=1.2){
zcp=find_glob_minim(params, s, F, zpt);
fprintf(test,"%.7f %.7f %.7f\n",zpt, zcp, db_dist(zcp,&params));
zpt+=dx;
}//zpt
fclose(test);
*/
/*
params.zpt=0.5;
params.rpt=0.03;
zcp=find_glob_minim(params,s,F,0.7);
printf("zcp is :%f\n",zcp);
*/


gsl_root_fsolver_free (s);
return 0;

}//end main

int find_deriv_zero(double x_l, double x_h, gsl_root_fsolver * s, gsl_function &F, db_params * params, double &r){

double x_lo=x_l, x_hi=x_h;
int iter=0;
int status;
F.params=params;

//Initialize state s
gsl_root_fsolver_set (s, &F, x_lo, x_hi);

//printf ("using %s method\n", gsl_root_fsolver_name (s));
     
//printf ("%5s [%9s, %9s] %9s %9s\n","iter", "lower", "upper", "root", "err(est)");


do{
	iter++;
	//perform an iteratin using method T
	status = gsl_root_fsolver_iterate (s);
	r = gsl_root_fsolver_root (s);
	x_lo = gsl_root_fsolver_x_lower (s);
	x_hi = gsl_root_fsolver_x_upper (s);
	//test convergence
	status = gsl_root_test_interval (x_lo, x_hi,epsabs,epsrel);
 
	if (status == GSL_SUCCESS){
//		printf ("Converged:\n");
 
//		printf ("%5d [%.7f, %.7f] %.7f %.7f\n",iter, x_lo, x_hi,r,x_hi - x_lo);
	}
	if(iter >= max_iter){printf("didn't converge \n");exit(0);}
}
while (status == GSL_CONTINUE && iter < max_iter);

return status; 
}

//FUNCTION TO FIND THE GLOBAL MINIMUM
// The interval to search in is za,zb
double  find_glob_minim(db_params &params,gsl_root_fsolver *s, gsl_function F, double zpt){

double r,r1,r2,r3;
double z, zcp;


params.zpt=zpt;

//LOOK FOR A ZERO IN THE DERIVATIVE STARTING AT za
z=za+dz/2;
//If the distance is decreasing at za;
if(db_sq_dist_deriv(z,&params) < 0 ){
	//find initial bracket looking for a change of singn in the derivative starting in za
	while(z<zb && db_sq_dist_deriv(z,&params) < 0  ){z+=dz;}
	//if found a change of sign in the derivative, minimize
	if(db_sq_dist_deriv(z,&params)>0){     
		find_deriv_zero(z-dz, z, s,  F, &params, r);
		r1=r;
	}
    else r1=zb;
	
}
//if the distance is increasing at za
else r1=za;

//LOOK FOR A ZERO IN THE DERIVATIVE STARTING AT zb
z=zb-dz/2;
//printf("derivative at zb is %f\n",db_sq_dist_deriv(z,&params));
//If the distance is increasing at zb
if(db_sq_dist_deriv(z,&params) > 0 ){
	//find initial bracket looking for a change of sign in the derivative starting in zb
	while(z>za && db_sq_dist_deriv(z,&params) > 0  ){z-=dz;}
	//if found a change of sign in the derivative, minimize
//	printf("z2 is %f\n",z);
	if(db_sq_dist_deriv(z,&params)<0){     
		find_deriv_zero(z,z+dz, s,  F, &params, r);
		r2=r;
	}
    else r2=za;
}
//if distance is decreasing at zb
else r2=zb;

//LOOK FOR A ZERO IN THE DERIVATIVE STARTING AT (za+zb)/2
z=(za+zb)/2;
//If the distance is decreasing at (za+zb)/2
if(db_sq_dist_deriv(z,&params) <= 0 ){
	//find initial bracket looking for a change of sign in the derivative
	while(z<zb && db_sq_dist_deriv(z,&params) < 0  ){z+=dz;}
	//if found a change of sign in the derivative, minimize
	if(db_sq_dist_deriv(z,&params)>0){     
		find_deriv_zero(z-dz, z, s,  F, &params, r);
		r3=r;
	}
    else r3=zb;
	
}
//If the distance is increasing at (za+zb)/2

else if(db_sq_dist_deriv(z,&params) > 0 ){
	//find initial bracket looking for a change of sign in the derivative
	while(z>za && db_sq_dist_deriv(z,&params) > 0  ){z-=dz;}
	//if found a change of sign in the derivative, minimize
	if(db_sq_dist_deriv(z,&params)<0){     
		find_deriv_zero(z,z+dz, s,  F, &params, r);
		r3=r;
	}
    else r3=za;
}



//choose which of the roots found is the global minimum
if(db_dist(r2,&params) < db_dist(r1,&params) ){zcp=r2;}
else zcp=r1;
if(db_dist(r3,&params) < db_dist(zcp,&params) ){zcp=r3;}

//if(zpt<0){zcp*=-1;}
//reset params.zpt 
params.zpt=zpt;
return zcp;
}
