#ifndef HEADER_H
#define HEADER_H 

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#define PI 4.0*atan(1.0)
#define pol 4 //pol is the number of points = order of interpolation convergence. pol-1=p the grade of the polynomial used for interpolation. Due to the laplacian in the equations, the error associated to interpolation is O(p-1)=O(pol-2)
#define dif 6 //number of points in the differentiation stencil (not including the center)
//Jian-geng modified L3, L2, L1, m3
#define L3 9.0 //half height of the box
#define L1 3.2 
#define L2 3.2
#define m3 180
#define dx (2*L3/(m3-1))

#define m2 (lrint(2*L2/dx) + 1)
#define m1 m2
#define ngridpts m1*m2*m3
#define R 1.0
#define disp 2 //Number of positions that vertex will be displaced when trying to find and extrapolation stencil

//Jian-geng modified circle+sphere parameters and za, zb
//radius of circle, long and short arm of the ellipse, 
#define acs 3
#define bcs 6
#define ccs 2
//displacement of the center of the circle and ellipse from 0
#define dcs 5.5
#define ecs -2.5
//displacement of the connection point from 0
#define fcs -3
#define za -8.5
#define zb 8.5
//for mass calculation
#define Nth (Nphi/2)
#define Nr 30
#define Ndv (Nr*Nphi*Nth) //Number of volume elements
#define Nda (Nphi*Nth)
//number of parametric coordinates for ploting
#define ntheta 50
#define nz 2*ntheta
#define Ngr (ntheta*nz)

//Jian-geng modified db_params
struct db_params{
double a,b,c,d,e,f,rpt,zpt;
};

//Radius of the dumbbell as a function of z 
double db_rad(double z, void * params);
//Distance between (rpt, zpt) and the point (z, r(z)) in the dumbbell
double db_dist(double z, void * params);
//Squared distance derivative of db_dist as a function of z
double db_sq_dist_deriv(double z, void * params);



void closest_point(int Num,int U_evolve[],double x[],double y[],double z[],double xc[],double yc[],double zc[],int** map,double r,int **Cen, double ** cp_data);
void interp_stencil(int poln, double xc, double yc, double zc, int X_interp[], int Y_interp[], int Z_interp[], double x_interp[], double y_interp[], double z_interp[]);
void vertex_stencil(int** Cen, int Num,double x[],double y[],double z[],double xcp[],double ycp[],double zcp[],int** Ver,int * inside );
void U_band(int U_evolve[],int indx_in_U[],int& N,int band_ini[],int nb, double x[], double y[], double z[],int ** map, double ** cp_data);
void U_close_to_surface(int U_evolve[], int indx_in_U[], int& N, double dist[]);
void ghost_points(int* U_ghost,int indx_ghost[],int& Ng,int U_evolve[],int indx_in_U[],int N,int ** map);
void U_update_ghost(int U_evolve[],int indx_in_U[],int& N,int **Cen_g,int Ng);
void U_update_set(int U_evolve[], int indx_in_U[], int& N,double xc[], double yc[], double zc[],int Nset);
double *weight3d(double Xs[],double Ys[],double Zs[],double xc,double yc,double zc,int len_xs);
void Get_E(int N,int Ng, int **Center,int **Cen_g, double x[],double y[], double z[], double xc[],double yc[],double zc[], double xc_g[],double yc_g[],double zc_g[],int indx_in_U[]);
void Get_Delta_h(int N,int indx_in_U[], int indx_ghost[], int ** map, int U_evolve_new[]);
void Get_E_vext_band(int Nv_ext,int **Cen_Vext,  double xc_Vext[],double yc_Vext[],double zc_Vext[],int indx_into_U[],int N);
void Get_E_set_vint(char filename[],int Nset,int **Cen_set, double x[],double y[], double z[], double xcp_set[],double ycp_set[],double zcp_set[],int indx_in_Vint[],int Nv_int, double ** cp_data, int * inside);
void Get_Mi_vext_vint(int Nv_ext,int **Cen_Vext, double x[],double y[], double z[], double xcp_Vext[],double ycp_Vext[],double zcp_Vext[],int indx_in_Vint[],int Nv_int, int V_ext[],int **map,double ** cp_data, int * inside);
void Get_Delta_h_v(int Nv_tot, int Nv_int, int N_ext, int indx_into_V_tot[], int ** map, int V_tot[]);
void print_to_file(double x[], double y[], double z[], double xc[],double yc[],double zc[],int V_tot[], int V_ext[], int V_int[], int U_evolve[],int Nv_tot, int Nv_ext, int Nv_int,int N);
void Get_IPM(double xp[],double yp[],double zp[],int Npos,int indx_in_comp_set[],int Ncs,char filename[]);
void grid_plotting(double xgr[], double ygr[], double zgr[]);
#endif

