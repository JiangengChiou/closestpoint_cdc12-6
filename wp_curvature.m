%clear;
%diverges with dt=1e-3
dt=1.0e-3;
T=round(4/dt);
uthreshold=1.;
%
disp('load E_vext_band...');
E_vext_band=load('./data3d/E_vext_band');
E_vext_band=spconvert(E_vext_band);

disp('load E_vext_vint...');
E_vext_vint=load('./data3d/E_vext_vint');
E_vext_vint=spconvert(E_vext_vint);

disp('load E_band_vint...');
E_band_vint=load('./data3d/E_band_vint');
E_band_vint=spconvert(E_band_vint);

disp('load Mi_vext_vint...');
Mi_vext_vint=load('./data3d/Mi_vext_vint');
Mi_vext_vint=spconvert(Mi_vext_vint);

disp('load E...');
E=load('./data3d/E_matrix');
E=spconvert(E);

disp('load Delta_h...');
Delta_h=load('./data3d/Delta_h');
Delta_h=spconvert(Delta_h);

disp('load Delta_v...');
Delta_v=load('./data3d/Delta_h_v');  
Delta_v=spconvert(Delta_v);

Num=size(E,2);

%Eb multiplies just the points in the computational band without the ghost points
Eb=E(1:Num,:);

%Delta_h doesn't have the diagonal, heare it is added after multiplying by E
M=sparse(Delta_h*E)-6*speye(Num);
clear Delta_h;

dist=load('./data3d/dist');

x=load('./data3d/Gridx_points');
y=load('./data3d/Gridy_points');
z=load('./data3d/Gridz_points');
m1=size(x,1);
m2=size(y,1);
m3=size(z,1);

V_tot=load('./data3d/V_tot'); %Exterior points
V_int=load('./data3d/V_int'); %V interior points, these are evolved
U_evolve=load('./data3d/U_evolve'); %Computational band
V_ext=load('./data3d/V_ext');

N=size(U_evolve,1);
Nv_int=size(V_int,1);
Nv_ext=size(V_ext,1);

%Grid definition
[X,Y,Z]=ndgrid(x,y,z);
[phi,th,rad]=cart2sph(X,Y,Z);
th = -th+pi/2;

%Interpolation matrix to evaluate function along s to track wave 
IP_ds_N=load('./data3d/IP_ds_N');
IP_ds_N=spconvert(IP_ds_N);
Nds=size(IP_ds_N,1);


%FOR PLOTTING:
%Interpolation matrix to estimate values of the function at surface grid  
disp('load IP_grid_N...');
IP_grid_N=load('./data3d/IP_grid_N');
IP_grid_N=spconvert(IP_grid_N);
%check definitions in c header file
thg=load('./data3d/thgrid.dat');
zg=load('./data3d/zgrid.dat');
rg=load('./data3d/rgrid.dat');
nz=size(zg,1);
ntheta=size(thg,1);
%rgrid and zgrid should have the same size 
[thgrid, zgrid]=meshgrid(thg,zg);
[thgrid, rgrid]=meshgrid(thg,rg);
xp=rgrid.*cos(thgrid);
yp=rgrid.*sin(thgrid);
zp=zgrid;

u0_grid=zeros(nz,ntheta);
u_grid=zeros(nz,ntheta);


%Constants definition 
dx=x(2)-x(1);

%L=1um
%tau=10s

Da=1;
Dv=100;
d=10;
g=10;
k=0.67;
K=1;
hc=3;
b=1;

M=Da/dx^2*M;
Dv_vint_vtot=Dv/dx^2*Delta_v;
clear Delta_v;
%}
%if the if all unactive protein is in bulk:
% Dv*dv/dn = -f
% dv/dn = -f/Dv
%
ktU=2*exp(-(th(U_evolve)).^6/(15*pi/180)^6);
ktVext=2*exp(-(th(V_ext)).^6/(15*pi/180)^6);

%INITIAL CONDITIONS
%u0=zeros(N,1)+0.2245 + 2*exp(-(th(U_evolve)).^6/(35*pi/180)^6) ;
u0=zeros(N,1)+0.1962 + (4-0.1962)*heaviside1(-th(U_evolve)+30*pi/180 ) ;
v0=zeros(m1*m2*m3,1)+2.63;
v1=zeros(m1*m2*m3,1);
%u_cp_vext=E_vext_band*u0;
%kvext=(k + ktVext + g*(u_cp_vext.^hc)./(K^hc+u_cp_vext.^hc));
%v0(V_ext)=Mi_vext_vint*v0(V_int) - 2*dist.*(kvext.*(E_vext_vint*v0(V_int)) - d*u_cp_vext)/Dv;   

u_t=u0;
v_t=v0;

samp=1;
u_samp=u0;

tk=round(0.1/dt);

flagds=zeros(1,Nds);
timeds=zeros(1,Nds);

%repmat(dist,1,Nv_int).*E_vext_vint is equivalent to diag(dist)*E_vext_vint

%w_t=[u_t;v_t(V_int)];
%


Buu=M-d*Eb;
diag_dist=spdiags(dist,0,Nv_ext,Nv_ext);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%TIME LOOP
disp('evolving...');
for itime=1:T
	
%REQUIRED QUANTITIES 
		

%TIME STEP

%Forwar Euler
%u_cp=Eb*u_t;
%u_cp_vext=E_vext_band*u_t;
%f= (k + ktU*heaviside(-itime+tk)  + g*(u_cp.^hc)./(K^hc+u_cp.^hc)).*(E_band_vint*v_t(V_int))- d*u_cp;
%f= (k  + g*(u_cp.^hc)./(K^hc+u_cp.^hc))*(veq)- d*u_cp;
%u_t = u_t + dt*M*u_t + dt*f;
%v_t(V_int) = v_t(V_int) + dt*Dv_vint_vtot*v_t(V_tot); 

%u_cp_vext=E_vext_band*u_t;
%k_vext=(k + ktVext*heaviside(-itime+tk) + g*(u_cp_vext.^hc)./(K^hc+u_cp_vext.^hc));

%v_t(V_ext)=Mi_vext_vint*v_t(V_int) -2*dist.*(kvext.*(E_vext_vint*v_t(V_int)) - d*u_cp_vext)/Dv;   

%---------------------------------------------------------------------------------
%IMEX Euler



%rate of Rho activation "non linear part of the model"
%u_t=w_t(1:N);
u_cp=Eb*u_t;
u_cp_vext=E_vext_band*u_t;
k_vext=b*(k + g*(u_cp_vext.^hc)./(K^hc+u_cp_vext.^hc));
k_band=b*(k + g*(u_cp.^hc)./(K^hc+u_cp.^hc));

%Sparse diagonal matrices

diag_k_vext=spdiags(k_vext,0,Nv_ext,Nv_ext);
diag_k_band=spdiags(k_band,0,N,N);

%B=model matrix

% du/dt = Buu*u +Buv(u)*v 
% dv/dt = Bvu*u +Bvv*v

%vtot=[vint ;
%      vgh]	
%vgh= vmi + 2*dist*dv/dz
%Dv*dv/dz=-f
%f= k(u)*v - d*u   

%vgh= vmi - 2*dist/Dv*k(u)*v + 2*dist/Dv*d*u

%Vgh_vext_vint= Mi_vext_vint-2*diag_dist*diag_k_vext*E_vext_vint/Dv
%Vgh_vext_band= 2*diag_dist*d/Dv*E_vext_band


%Buu= M + FL_band
%Buv= FNL(u)_band_vint + F_band_vint


%Bvu= Dv_vint_vtot*[0_vint_band
%                   Vgh_vext_band ] 

%Bvv = Dv_vint_vtot* [I_vint ; 
%                     Vgh_vext_vint ]  

%Bv= Dv_vint_vtot*[0_vint_band  , I_vint ;
%                  Vgh_vext_band, Vgh_vext_vint] 


%Not solving for v and u simulataneously, using w, B and A because it takes more time than solving for u and v independently
%Bv=Dv_vint_vtot*[sparse(Nv_int,N),             speye(Nv_int); 
%                 2*diag_dist*d/Dv*E_vext_band, Mi_vext_vint-2*diag_dist*diag_k_vext*E_vext_vint/Dv];
%B=[Buu, diag_k_band*E_band_vint ;
%      Bv ] ;
%A=speye(N+Nv_int)-dt*B;

%-------------------------------------------------------------------------------------------------
%A=method matrix

%u_(t+1)-u_t=dt*Buu*(u_(t+1)) + dt*Buv(u_t)*v_t
% (I-dt*Buu)*u_(t+1)=u_t + dt*Buv(u_t)*v_t
%         Au*u_(t+1)=bu

Buv=diag_k_band*E_band_vint;
Au=speye(N)-dt*Buu;
bu=u_t+dt*Buv*v_t(V_int);

%Av=I-dt*Bvv
%bv=v_t+dt*Bvu*u_t

bv=v_t(V_int)+dt*Dv_vint_vtot*([zeros(Nv_int,1); 
	               2*d/Dv*dist.*(E_vext_band*u_t) ]  );
Av=speye(Nv_int)-dt*Dv_vint_vtot*[speye(Nv_int) ;
	Mi_vext_vint-2*diag_dist*diag_k_vext*E_vext_vint/Dv];

u_t=gmres(Au,bu,15,1e-8,25);
v_t(V_int)=gmres(Av,bv,25,1e-6,35);

	if mod(itime,10)==0
		itime	
		max(u_t)
		mean(u_t)*100
		min(u_t)
		max(v_t(V_int))
		mean(v_t(V_int))
		min(v_t(V_int))
	end;

%WAVE TRACKING

        for nds=1:Nds
                if flagds(nds)==0
                        if IP_ds_N(nds,:)*u_t > uthreshold
                                flagds(nds)=1;
                                timeds(nds)=itime;
                        end;
                end;
        end;
%VISUALIZATION
	if mod(itime,10)==0
	%
		u_samp=[u_samp u_t];
		samp=samp+1;
%
		u_grid_vect=IP_grid_N*u_t;
		% make grid matrix with the value of the function from vector	
		% in c file: n = j*ntheta + i
		for n=0:(ntheta*nz-1)
			ii=mod(n,ntheta)+1;
			jj=fix(n/ntheta)+1;
			u_grid(jj,ii)=u_grid_vect(n+1);
		end;
		surf(xp,yp,zp,u_grid,'FaceColor','interp','EdgeColor','none','FaceLight','phong')
		view(10,0);
		axis equal
		caxis([0.1474 2.01])
		drawnow
	%
	
	plot(IP_ds_N*u_t)
	axis([0 Nds 0 4])
	drawnow
	end;
end;%end time evolution

timevsds=timeds*dt;
save data3d/timevsds.dat timevsds  -ASCII;

%MOVIE


usamp2=u_samp(:,1);

for i=1:5:size(u_samp,2)
usamp2=[usamp2 u_samp(:,i)];
end;


j=1;
for ind=1:size(usamp2,2)
	u_grid_vect=IP_grid_N*usamp2(:,ind);
		%index in grid = n = j*ntheta + i
		%j -> z, i->theta
		for n=0:(ntheta*nz-1)
			ii=mod(n,ntheta)+1;
			jj=fix(n/ntheta)+1;
			%makes actual matrix grid
			u_grid(jj,ii)=u_grid_vect(n+1);
		end;
		surf(xp,yp,zp,u_grid,'FaceColor','interp','EdgeColor','none','FaceLight','phong')
		view(0,-30);
		axis equal
		Mov(j)=getframe;
		j=j+1;
end;

movie2avi(Mov,'anim2.avi','compression','None')

%THETA PROFILE EVOLUTION

j=1;
for ind=1:5:size(u_samp,2)*0.7
	u_grid_vect=IP_grid_N*u_samp(:,ind);
		%index in grid = n = j*ntheta + i
		%j -> z, i->theta
		for n=0:(ntheta*nz-1)
			ii=mod(n,ntheta)+1;
			jj=fix(n/ntheta)+1;
			%makes actual matrix grid
			u_grid(jj,ii)=u_grid_vect(n+1);
		end;
		%u_th is the average over phi
		u_th=mean(u_grid,2);
		plot(u_th)
		axis([0 100 0 1.5]);	
		Mov_th(j)=getframe;
		j=j+1;
end;



