#include "header_extrp.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//to compile: g++ main_extrp.c functions_extrp.c -o matr

int main(){
int ngridpts_file;	
double ** cp_data;
double *x,*y,*z,*dist;
int * inside;
int *U_evolve;//U computational band
int *V_int; // Points inside the surface
int *U_exterior; //U computational band exterior
int *V_tot;//V_int + V_ext
int * indx_into_V_tot; indx_into_V_tot=(int *)malloc(ngridpts*sizeof(int));
int i,j,k,ix,iy,iz,jdx,Nv_int,N_ext,g_indx,N,n,Nv_tot;
double Xsmall[pol],Ysmall[pol],Zsmall[pol];
double *p;
int * band_ini; band_ini=(int *)malloc(ngridpts*sizeof(int));
int nb;
int *indx_in_U; indx_in_U=(int *) malloc ((ngridpts)*sizeof(int));
int *indx_in_Vint; indx_in_Vint=(int *) malloc ((ngridpts)*sizeof(int));
int  **Cen; //integer coordinates of closest points of U 
int  **Cen_g; //ghost closest points
int **map;
int ngridpts_data;

printf("dx %f\n",dx);

//for each grid point cp_data contains the three coordinates of the cp
cp_data=(double **)malloc(ngridpts*sizeof(double *));
for(i=0;i<ngridpts;i++){cp_data[i]=(double *)malloc(3*sizeof(double));}

map=(int **)malloc(ngridpts*sizeof(int *));
for(i=0;i<ngridpts;i++){map[i]=(int *)malloc(3*sizeof(int));}

//positions of the closest points in U_evolve
double *xc,*yc,*zc;

int *indx_ghost; indx_ghost=(int*)malloc(ngridpts*sizeof(int));
int *indx_in_Vext;indx_in_Vext=(int*)malloc(ngridpts*sizeof(int));

int *U_ghost;
int *V_ext; //The V ghost points 

//Number of U and V ghost points:
int Ng;
int Nv_ext;

x=(double *) malloc ((m1)*sizeof(double));
y=(double *) malloc ((m2)*sizeof(double));
z=(double *) malloc ((m3)*sizeof(double));
dist=(double *) malloc ((ngridpts)*sizeof(double));
inside=(int *) malloc ((ngridpts)*sizeof(int));

U_evolve=(int *) malloc ((ngridpts)*sizeof(int));
U_exterior=(int *) malloc ((ngridpts)*sizeof(int)); 
V_int=(int *) malloc ((ngridpts)*sizeof(int)); 
FILE * file_cp;

//Read cp_data, dist, inside
if ((file_cp=fopen("./data3d/cp_data.dat","r"))==NULL) {printf("cannot open cp_data file\n"); exit(0); }
fscanf(file_cp,"%d",&ngridpts_data);
if(ngridpts_data != ngridpts){printf("incompatible number of grid points \n");exit(0);}

for (i=0; i<ngridpts; i++) {
	fscanf(file_cp,"%lf %lf %lf %lf %d",&cp_data[i][0],&cp_data[i][1],&cp_data[i][2], &dist[i], &inside[i]);
	if(inside[i]!=1 && inside[i]!=0){printf("inside[] is not well defined\n");exit(0);}
}
fclose(file_cp);

printf("total grid points %d\n",ngridpts);
		
//set grid points
for(i=0;i<m1;i++){x[i]=-L1+dx*i;}
for(i=0;i<m2;i++){y[i]=-L2+dx*i;}
for(i=0;i<m3;i++){z[i]=-L3+dx*i;}

//initialize indx_in_Vint
for (i=0; i<ngridpts; i++) {
        indx_in_Vint[i]=-1;
}
   
//Makes band with points within 5*dx from surface
//Makes V_int, map 

FILE * band_file;
//used to plot and test wether the calculation of cp is working
band_file=fopen("band_data.dat","w");
nb=0;
Nv_int=0;
for (k=0;k<m3;k++){
	for (i=0; i<m1; i++){
		for (j=0; j<m2; j++) {
		//gets global index from integer coordinates
		g_indx=k*m2*m1+i*m2+j;
		//map:global index --> integer coordinates
		map[g_indx][0]=i;
		map[g_indx][1]=j;
		map[g_indx][2]=k;
		//distance from surface was imported from file
		if (dist[g_indx]<5*dx){
			band_ini[nb]=g_indx;
			nb++;
			fprintf(band_file,"%f %f %f\n",x[i],y[j],z[k]);	
		}
		//if the grid point lies inside the volume include in V_int
		if (inside[g_indx]==1) {
			V_int[Nv_int]=g_indx;
			indx_in_Vint[g_indx]=Nv_int;
			Nv_int++;
			}
		}//j
	}//i
}//k
fclose(band_file);
printf("Interior points: %d\n",Nv_int);

//initialize U_evolve and indx_in_U
N=0; 
for (i=0; i<ngridpts; i++) {
	U_evolve[i]=0;
	indx_in_U[i]=-1;
}

//First estimation of U_evolve based on the interp stencil of cp of  points within a distance 5*dx from the surface.
U_band(U_evolve, indx_in_U, N, band_ini,nb,x,y,z,map,cp_data);

//Include in U_evolve the interpolation stencil of all the points within sqrt(3)*dx/2 from the surface
//U_close_to_surface(U_evolve, indx_in_U, N, dist);

//used to plot and test whether the calculation of cp is working
FILE * fband_file;
fband_file=fopen("fband_data.dat","w");
for(i=0;i<N;i++){
fprintf(fband_file,"%f %f %f\n",x[map[U_evolve[i]][0]],y[map[U_evolve[i]][1]],z[map[U_evolve[i]][2]]);
}
fclose(fband_file);	

//get positions (xgr, ygr,zgr) defined over a parametric rectangular set (theta,z)
double xgr[ntheta*nz], ygr[ntheta*nz], zgr[ntheta*nz];
grid_plotting(xgr,ygr,zgr);



//check whether "gr" interpolation stencil is not in U and add it
U_update_set(U_evolve,indx_in_U,N,xgr,ygr,zgr,Ngr);
printf("Points in U after gr: %d\n",N);

//Import ds values for tracking wave
//FILE * ds_pos_file;
double *xds, *yds, *zds;
int Nds;
//if ((ds_pos_file=fopen("./data3d/ds_pos.dat","r"))==NULL) {printf("cannot open ds_pos file\n");}
//fscanf(ds_pos_file,"%d",&Nds);
xds=(double *)malloc(Nds*sizeof(double));
yds=(double *)malloc(Nds*sizeof(double));
zds=(double *)malloc(Nds*sizeof(double));
//for (i=0; i<Nds; i++) {	fscanf(ds_pos_file,"%lf %lf",&zds[i],&yds[i]); 	xds[i]=0; }
//fclose(ds_pos_file);

//check whether the ds positions  have interpolation points not added to U_evolve
//U_update_set(U_evolve,indx_in_U,N,xds,yds,zds,Nds);
printf("Points in U after ds %d\n",N);


//find ghost points
U_ghost=(int *)malloc(N*sizeof(int));
ghost_points(U_ghost,indx_ghost,Ng,U_evolve,indx_in_U,N,map);

//Find V_ext, the same as V ghost points
printf("V ghost points\n");
V_ext=(int *)malloc(Nv_int*sizeof(int));
ghost_points(V_ext,indx_in_Vext, Nv_ext, V_int,indx_in_Vint,Nv_int,map );

//calculate ghost points' closest points
	
Cen_g=(int **)malloc(Ng*sizeof(int *));
for (i=0; i<Ng; i++) {
	Cen_g[i]=(int *)malloc(3*sizeof(int));
}   

double xc_g[Ng],yc_g[Ng],zc_g[Ng];
closest_point(Ng,U_ghost,x,y,z,xc_g,yc_g,zc_g,map,R,Cen_g,cp_data);

//check whether the ghost points have interpolation points not added to U_evolve
U_update_set(U_evolve,indx_in_U,N,xc_g,yc_g,zc_g,Ng);
printf("Points in U after ghost %d\n",N);


//calculate the final band points' closest points

//Get closest points of U_evolve
Cen=(int **)malloc(N*sizeof(int *));
for (i=0; i<N; i++) {
	Cen[i]=(int *)malloc(3*sizeof(int));
}   
xc=(double *) malloc (N*sizeof(double));
yc=(double *) malloc (N*sizeof(double));
zc=(double *) malloc (N*sizeof(double));

closest_point(N,U_evolve,x,y,z,xc,yc,zc,map,R,Cen,cp_data);

//Calculate U_exterior
N_ext=0;
for(n=0;n<N;n++){
	g_indx=U_evolve[n];
	//If point outside surface
	if(inside[g_indx]==0){
		U_exterior[N_ext]=g_indx;
		N_ext++;
	}
}
printf("Points in U_exterior: %d\n",N_ext);

//Make V_tot (V_int + V_ext)

for(i=0;i<ngridpts;i++){indx_into_V_tot[i]=-1;}

Nv_tot=Nv_int+Nv_ext;
V_tot=(int *) malloc ((Nv_int+Nv_ext)*sizeof(int));

for(n=0;n<Nv_int;n++){
        V_tot[n]=V_int[n];
        indx_into_V_tot[V_int[n]]=n;
}
for(n=0;n<Nv_ext;n++){
        V_tot[n+Nv_int]=V_ext[n];
        indx_into_V_tot[V_ext[n]]=n+Nv_int;
}

printf("Points in V_tot: %d\n",Nv_tot);

//find closest point centers for V_ext    
double *xc_Vext,*yc_Vext,*zc_Vext;
xc_Vext=(double *)malloc(Nv_ext*sizeof(double));
yc_Vext=(double *)malloc(Nv_ext*sizeof(double));
zc_Vext=(double *)malloc(Nv_ext*sizeof(double));

int  **Cen_Vext; //closest points of V_ext	
Cen_Vext=(int **)malloc(Nv_ext*sizeof(int *));
for (i=0; i<Nv_ext; i++) {
	Cen_Vext[i]=(int *)malloc(3*sizeof(int));
}   	

closest_point(Nv_ext,V_ext,x,y,z,xc_Vext,yc_Vext,zc_Vext,map,R,Cen_Vext,cp_data);  

//makes file with distance of exterior points from surface
FILE *fdist;

if ((fdist=fopen("./data3d/dist","w"))==NULL) {
	printf("cannot open print file\n");
	exit(0);
}

for (i=0; i<Nv_ext; i++) {
	fprintf(fdist,"%.12e\n",dist[V_ext[i]]);
}
fclose(fdist);

//get matrix to get values of function at the grid positions for plotting
printf("calculating IP_grid_N...\n");
char IP_grid_N_name[]="IP_grid_N";
Get_IPM(xgr, ygr, zgr, Ngr, indx_in_U, N, IP_grid_N_name);

//printf("%d %f %f %f %f %f %f\n", nds, xds[0],yds[0],zds[0],xds[nds-1],yds[nds-1],zds[nds-1]);

//get matrix to get values of function at ds positions to track wave  
printf("calculating IP_ds_N...\n");
//char IP_ds_N_name[]="IP_ds_N";
//Get_IPM(xds, yds, zds, Nds, indx_in_U, N, IP_ds_N_name);

print_to_file(x,y,z,xc,yc,zc,V_tot,V_ext,V_int,U_evolve,Nv_tot,Nv_ext, Nv_int,N);
	
printf("calculating E...\n");
Get_E(N,Ng,Cen,Cen_g,x,y,z,xc,yc,zc,xc_g,yc_g,zc_g,indx_in_U);
	
printf("calculating Delta_h...\n");
Get_Delta_h(N,indx_in_U,indx_ghost,map,U_evolve);

printf("calculating E_vext_band...\n");
Get_E_vext_band(Nv_ext, Cen_Vext, xc_Vext, yc_Vext, zc_Vext, indx_in_U,N);


char E_vext_vint_name[]="E_vext_vint";
printf("calculating E_vext_vint...\n");
Get_E_set_vint(E_vext_vint_name,Nv_ext, Cen_Vext, x, y, z, xc_Vext, yc_Vext, zc_Vext, indx_in_Vint,Nv_int,cp_data,inside);

char E_band_vint_name[]="E_band_vint";
printf("calculating E_band_vint...\n");
Get_E_set_vint(E_band_vint_name,N, Cen, x, y, z, xc, yc, zc, indx_in_Vint,Nv_int, cp_data, inside);

printf("calculating Mi_vext_vint...\n");

//It calculates Mi_ext_tot (the matrix to estimate the mirror point)
Get_Mi_vext_vint(Nv_ext,Cen_Vext, x, y, z, xc_Vext, yc_Vext, zc_Vext, indx_in_Vint, Nv_int, V_ext,map, cp_data, inside);    
   
printf("calculating V Laplacian matrix...\n");
Get_Delta_h_v(Nv_tot, Nv_int, N_ext, indx_into_V_tot, map, V_tot);

for (i=0; i<Nv_ext; i++) {free(Cen_Vext[i]);	}  
free(Cen_Vext);

for (i=0; i<Ng; i++) {free(Cen_g[i]);}  
free(Cen_g);

for (i=0; i<N; i++) {free(Cen[i]);}  
free(Cen);

for (i=0; i<ngridpts; i++) {free(map[i]);}  
free(map);
free(indx_ghost); free(indx_into_V_tot);
free(x);free(y);free(z);
free(dist);
free(V_int);  
free(V_ext);
free(U_exterior);
free(U_evolve);

printf("End main\n"); 
return 0;
}//END MAIN

