#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "header_extrp.h"



void closest_point(int Num,int U_evolve[],double x[],double y[],double z[],double xc[],double yc[],double zc[],int ** map,double r,int** Cen, double ** cp_data  ){
//given total points in bands and the surface, calculate closest points
int l,tmp,i,j,k;
for (l=0;l<Num;l++){
	tmp=U_evolve[l];
	xc[l]=cp_data[tmp][0];
	yc[l]=cp_data[tmp][1];       
	zc[l]=cp_data[tmp][2];
	Cen[l][0]=lrint((xc[l]+L1)/dx);
	Cen[l][1]=lrint((yc[l]+L2)/dx);
	Cen[l][2]=lrint((zc[l]+L3)/dx);
		if(x[Cen[l][0]]<(-L1) && y[Cen[l][1]]<(-L2)){
		printf("%f %f %f %f %d\n",x[Cen[l][0]],y[Cen[l][1]],xc[l],yc[l],tmp);
		exit(1);
		}	
	}
}

void interp_stencil(int poln, double xc, double yc, double zc, int X_interp[], int Y_interp[], int Z_interp[], double x_interp[], double y_interp[], double z_interp[]){

int i, Xc,Yc,Zc;
double xXc, yYc, zZc;

//Integer coordinates of the closest grid point to the position to interpolate
Xc=lrint((xc+L1)/dx);
Yc=lrint((yc+L2)/dx);
Zc=lrint((zc+L3)/dx);

//Real coordinates of the closest grid point to the position to interpolate
xXc=-L1+dx*Xc;
yYc=-L2+dx*Yc;
zZc=-L3+dx*Zc;


// if the number of points (poln) is odd
        if( poln%2 != 0){

                for(i=0;i<poln;i++){
                X_interp[i]= Xc-(floor(poln/2))+i;
                Y_interp[i]= Yc-(floor(poln/2))+i;
                Z_interp[i]= Zc-(floor(poln/2))+i;
                }

        }

//if pol is even
        if(poln%2==0){
                //The ifs are just to center the stencil as much as possible
                for(i=0; i<poln; i++){
                        if(xc >= xXc){
                        X_interp[i]=Xc - (poln/2 -1) + i;
                        }
                        else{
                        X_interp[i]=Xc - poln/2  + i;
                        }
                        if(yc >= yYc){
                        Y_interp[i]=Yc - (poln/2 -1) + i;
                        }
                        else{
                        Y_interp[i]=Yc - poln/2  + i;
                        }
                        if(zc >= zZc){
                        Z_interp[i]=Zc - (poln/2 -1) + i;
                        }
                        else{
                        Z_interp[i]=Zc - poln/2  + i;
                        }
                }
        }

//Real coordinates of the interpolation stencil
        for(i=0;i<poln;i++){
        x_interp[i]=-L1+dx*X_interp[i];
        y_interp[i]=-L2+dx*Y_interp[i];
        z_interp[i]=-L3+dx*Z_interp[i];

        }

}

void vertex_stencil(int** Cen, int Num,double x[],double y[],double z[],double xcp[],double ycp[],double zcp[],int** Ver,int * inside ){
    //given total points in bands and the surface, calculate closest points

int l,tmp,i,j,k,n,itemp,jtemp,ktemp, gl_temp;
double distcp,tempdist;
int tmpVer[3];

    for (l=0;l<Num;l++){

        tempdist=1000000;
        //Go through the 27 grid positions around Cen looking for the closest to cp inside the volume
        itemp=200;
        for(i=-1;i<2;i++){
        for(j=-1;j<2;j++){
        for(k=-1;k<2;k++){

        tmpVer[0]=Cen[l][0]+i;
        tmpVer[1]=Cen[l][1]+j;
        tmpVer[2]=Cen[l][2]+k;
                //if the grid point is inside volume
		gl_temp=tmpVer[2]*m1*m2 + tmpVer[0]*m2 + tmpVer[1];
                if(inside[gl_temp]==1){
                        //if the distance from the grid point to cp[l] is the shortest keep grid point (as nV) 
                        distcp = pow(x[tmpVer[0]]-xcp[l],2)+pow(y[tmpVer[1]]-ycp[l],2)+pow(z[tmpVer[2]]-zcp[l],2);
                        if(distcp<tempdist){
                        tempdist=distcp;
                        itemp=i; jtemp=j;ktemp=k;
                        }

                }
        }//k
        }//j
        }//i
        if(itemp==200){printf("Not possible to find a vertex for the extrapolation stencil inside V\n");
                printf("Cen is %f %f %f, dx is %f, l is %d\n",x[Cen[l][0]],y[Cen[l][1]],z[Cen[l][2]],x[1]-x[0],l);
                exit(0);
        }
        //The closest to cp from the adjecent grid points of Cen will be the vertex of the extrapolation stencil
        Ver[l][0]=Cen[l][0]+itemp;
        Ver[l][1]=Cen[l][1]+jtemp;
        Ver[l][2]=Cen[l][2]+ktemp;

    }//end for l 
}


void print_to_file(double x[], double y[], double z[], double xc[],double yc[],double zc[],int V_tot[], int V_ext[], int V_int[], int U_evolve[],int Nv_tot, int Nv_ext, int Nv_int,int N){

	FILE *fp_gridx,*fp_gridy,*fp_gridz,*fu,*fv, *fv_ext, *fv_int;
	int i;
	if ((fp_gridx=fopen("./data3d/Gridx_points","w"))==NULL){printf("cannot open file of gridx\n");exit(0);}
	if ((fp_gridy=fopen("./data3d/Gridy_points","w"))==NULL){printf("cannot open file of gridy\n");exit(0);}
 	if ((fp_gridz=fopen("./data3d/Gridz_points","w"))==NULL){printf("cannot open file of gridz\n");exit(0);}

	if ((fv=fopen("./data3d/V_tot","w"))==NULL){printf("cannot open file of center\n"); exit(0);}
	if ((fv_ext=fopen("./data3d/V_ext","w"))==NULL){printf("cannot open file of V_ext\n"); exit(0);}
	if ((fv_int=fopen("./data3d/V_int","w"))==NULL){printf("cannot open file of V_int\n"); exit(0);}
	if ((fu=fopen("./data3d/U_evolve","w"))==NULL){	printf("cannot open file of center\n"); exit(0);}

	for (i=0; i<m1; i++) { fprintf(fp_gridx,"%.12e\n",x[i]);}
	for (i=0; i<m2; i++) { fprintf(fp_gridy,"%.12e\n",y[i]);}
	for (i=0; i<m3; i++) { fprintf(fp_gridz,"%.12e\n",z[i]);}

	for (i=0; i<N; i++) {fprintf(fu,"%d\n",U_evolve[i]+1);}
  
	for (i=0; i<Nv_tot; i++) {  fprintf(fv,"%d\n",V_tot[i]+1); }
        for (i=0; i<Nv_ext; i++) {  fprintf(fv_ext,"%d\n",V_ext[i]+1); }
        for (i=0; i<Nv_int; i++) {  fprintf(fv_int,"%d\n",V_int[i]+1); }   

	fclose(fp_gridx); fclose(fp_gridy); fclose(fp_gridz);
	fclose(fu); fclose(fv); fclose(fv_ext);
}


void U_band(int U_evolve[],int indx_in_U[],int& N,int band_ini[],int nb, double x[], double y[], double z[],int ** map,double ** cp_data){

int X_interp[pol],Y_interp[pol],Z_interp[pol];
double x_interp[pol],y_interp[pol],z_interp[pol];
int i,j,ii,jj,kk,g_indx;
int n=0;
int ** Cen_band;
Cen_band=(int **)malloc(nb*sizeof(int *));
for (i=0; i<nb; i++) {Cen_band[i]=(int *)malloc(3*sizeof(int));}
double *xcb, *ycb, *zcb;
xcb=(double *)malloc(nb*sizeof(double));
ycb=(double *)malloc(nb*sizeof(double));
zcb=(double *)malloc(nb*sizeof(double));
closest_point(nb,band_ini,x,y,z,xcb,ycb,zcb,map,R,Cen_band,cp_data);

FILE * uband_file;
if((uband_file=fopen("./uband_data.dat","w"))==NULL){printf("cannot open uband_file\n");exit(0);}

for (i=0; i<nb; i++) {
	
	//get integer coordinates of the interpolation stencil of point i
	interp_stencil(pol, xcb[i], ycb[i], zcb[i], X_interp, Y_interp, Z_interp, x_interp, y_interp, z_interp);	
	for (kk=0; kk<pol; kk++) {
		for (ii=0; ii<pol; ii++) {
			for (jj=0; jj<pol; jj++){
				//get global index of each point in the interpolation stencil	
				//MATLAB'S ndgrid INCREASES FIRST IN X, THEN IN Y				
				g_indx=Z_interp[kk]*m2*m1+X_interp[ii]*m2+Y_interp[jj];
				//if the point has not been added to  U_evolve, add it
				if (indx_in_U[g_indx]==-1) {
				U_evolve[n]=g_indx;
				indx_in_U[g_indx]=n;
				n++;
				fprintf(uband_file,"%f %f %f\n",x[X_interp[ii]],y[Y_interp[jj]],z[Z_interp[kk]]);
				}//end if
			}//end jj
		}//end ii
	}//end kk
}//end i

fclose(uband_file);

//Update N value
N=N+n;
printf("total points in U_evolve based on cp of band:%d\n",N);

}

void U_close_to_surface(int U_evolve[], int indx_in_U[], int& N, double dist[]){
int i,j,k,p,ii,jj,kk,g_indx,g_indx_p,polmod;
int X_interp[pol],Y_interp[pol],Z_interp[pol];
int n=0;

for (k=0;k<m3;k++){
	for (i=0; i<m1; i++){
		for (j=0; j<m2; j++) {
			//get global index from integer coordinates
			g_indx=k*m2*m1+i*m2+j;

			//if the point is close enough to the surface
			if(dist[g_indx]<=sqrt(3)*dx/2){
				
				//get integer interpolation grid of g_indx
				//If pol is even, use the next odd number size for the interpolation stencil
				if(pol%2==0){polmod=pol+1;}
                                else{polmod=pol;}
				for (p=0; p<polmod; p++) {
					X_interp[p]=i-(lrint(pol/2))+p; 
					Y_interp[p]=j-(lrint(pol/2))+p;
					Z_interp[p]=k-(lrint(pol/2))+p;
				}//end p

				for (kk=0; kk<pol; kk++) {
					for (ii=0; ii<pol; ii++) {
						for (jj=0; jj<pol; jj++) {
							//get global index of each point in the interpolation stencil	
							//MATLAB'S ndgrid INCREASES FIRST IN X, THEN IN Y				
							g_indx_p=Z_interp[kk]*m2*m1+X_interp[ii]*m2+Y_interp[jj];
							//if the point has not been added to  U_evolve, add it
							if (indx_in_U[g_indx_p]==-1) {
								U_evolve[N+n]=g_indx_p;
								indx_in_U[g_indx_p]=N+n;
								n++;
							}//end if
						}//end jj
					}//end ii
				}//end kk
			}//end if close to surface
		}//j
	}//i
}//k

N=N+n;

printf("Points in U_evolve after close to surface: %d\n",N);
}//end of function

void ghost_points(int* U_ghost,int indx_ghost[],int& Ng,int U_evolve[],int indx_in_U[],int N,int ** map){

	int X_interp[pol],Y_interp[pol],Z_interp[pol];
	int i,k,g_indx,g_indx_U,ix,iy,iz;
	Ng=0;
	for(i=0;i<N;i++){U_ghost[i]=0;}
    int Xind[6]={0,0,-1,0,0,1},Yind[6]={0,-1,0,0,1,0},Zind[6]={-1,0,0,1,0,0};
	
	for (i=0; i<ngridpts; i++) {
		indx_ghost[i]=-1;
	}
	//for all points so far in U_evolve
    for (i=0; i<N; i++) {
		//global index of point in U
		g_indx_U=U_evolve[i];
		//get integer coordinates
		ix=map[g_indx_U][0];
		iy=map[g_indx_U][1];
        	iz=map[g_indx_U][2];
		for (k=0; k<dif; k++) {
			//go through differentiation stencil
			X_interp[k]=Xind[k]+ix;
			Y_interp[k]=Yind[k]+iy;
            		Z_interp[k]=Zind[k]+iz;
			//get global index of point in the diff stencil
			g_indx=Z_interp[k]*m2*m1+X_interp[k]*m2+Y_interp[k];
			//if the point is not in U_evolve
			if (indx_in_U[g_indx]==-1) {
				//if the point has not been added to ghost points
				if(indx_ghost[g_indx]==-1) {
					indx_ghost[g_indx]=Ng;
					U_ghost[Ng]=g_indx;
					Ng++;
				}//end if
			}//end if
		}//end k
	}
	printf("total ghost points: %d\n",Ng);
	
}	
void U_update_ghost(int U_evolve[],int indx_in_U[],int& N, double * xc_ghost, double *yc_ghost, double * zc_ghost,int Ng){
int X_interp[pol],Y_interp[pol],Z_interp[pol];
double x_interp[pol],y_interp[pol],z_interp[pol];
int i,j,ii,jj,kk,g_indx;
int n=0;

n=0;
for (i=0; i<Ng; i++) {

	interp_stencil(pol,xc_ghost[i], yc_ghost[i], zc_ghost[i], X_interp, Y_interp, Z_interp,x_interp,y_interp, z_interp );

	for (kk=0; kk<pol; kk++) {
		for (ii=0; ii<pol; ii++) {
			for (jj=0; jj<pol; jj++) {
			g_indx=Z_interp[kk]*m2*m1+X_interp[ii]*m2+Y_interp[jj];
				if (indx_in_U[g_indx]==-1) {
				U_evolve[N+n]=g_indx;
				indx_in_U[g_indx]=N+n;
				n++;
				}//end if
			}//end jj
		}//end ii
	}//end kk
}
//update N
N=N+n;
printf("Final total points in U_evolve after ghost: %d\n",N);
}

void U_update_set(int U_evolve[], int indx_in_U[], int& N,double xc[], double yc[], double zc[],int Nset){

int X_interp[pol],Y_interp[pol],Z_interp[pol];
double x_interp[pol],y_interp[pol],z_interp[pol];
int i,j,ii,jj,kk,g_indx;
int n=0;

printf("Nset is %d\n", Nset);

for (i=0; i<Nset; i++) {
        interp_stencil(pol, xc[i], yc[i],zc[i],X_interp,Y_interp, Z_interp, x_interp, y_interp, z_interp);

        for (kk=0; kk<pol; kk++) {
                for (ii=0; ii<pol; ii++) {
                        for (jj=0; jj<pol; jj++) {
                                //get global index  of each point in the interp stencil
                                g_indx=Z_interp[kk]*m1*m2+X_interp[ii]*m2+Y_interp[jj];
                                if (indx_in_U[g_indx]==-1) {
                                U_evolve[N+n]=g_indx;
                                indx_in_U[g_indx]=N+n;
                                n++;
                                }//end if
                        }//end jj
                }//end ii
        }//end kk
}
//update n
N=N+n;
}


void grid_plotting(double xgr[], double ygr[], double zgr[]){
//ntheta and nz are the number of coordinates 
int i,j,n;
//coordinates increments
double dth=2*PI/(ntheta-1);
//Jian-geng modified to not do mirroring in z
double dz=(zb-za)/(nz-1);
//Jian-geng modified db_params
db_params params={acs,bcs,ccs,dcs,ecs,fcs,0,0};

FILE * thgrid_file;
FILE * zgrid_file;
FILE * rgrid_file;
FILE * xyz_file;

if((xyz_file=fopen("./xyzgrid.dat","w"))==NULL){printf("Cannot open thgrid_file\n");exit(0);}
//get rectangular coordinates at each pair (i,j)
for(j=0;j<nz;j++){
	for(i=0;i<ntheta;i++){
		n = j*ntheta + i;
		xgr[n]=db_rad(j*dz+za,&params)*cos(i*dth);
		ygr[n]=db_rad(j*dz+za,&params)*sin(i*dth);
		zgr[n]=j*dz+za;
		fprintf(xyz_file,"%f %f %f\n",xgr[n],ygr[n],zgr[n]);
		}//end i
}//end j

//export vectors to make mesh in matlab
if((thgrid_file=fopen("./data3d/thgrid.dat","w"))==NULL){printf("Cannot open thgrid_file\n");exit(0);}
if((zgrid_file=fopen("./data3d/zgrid.dat","w"))==NULL){printf("Cannot open zgrid_file\n");exit(0);}
if((rgrid_file=fopen("./data3d/rgrid.dat","w"))==NULL){printf("Cannot open rgrid_file\n");exit(0);}

for(i=0;i<ntheta;i++){fprintf(thgrid_file,"%f\n",i*dth);}
for(i=0;i<nz;i++){fprintf(zgrid_file,"%f\n",i*dz+za);}
for(i=0;i<nz;i++){fprintf(rgrid_file,"%f\n",db_rad(i*dz+za,&params));}


fclose(thgrid_file);fclose(zgrid_file);fclose(rgrid_file);fclose(xyz_file);
}

//Matrix to get values of the  function at  each position (xp,yp,zp) using interpolation. 
//The interpolation matrix (IPM) multiplies a computational set 
void Get_IPM(double xp[],double yp[],double zp[],int Npos,int indx_in_comp_set[],int Ncs,char filename[]){

int *idx,*g_indx,i,j,ix,iy,iz,k,tmp;
int X_interp[pol],Y_interp[pol],Z_interp[pol];
double x_interp[pol],y_interp[pol],z_interp[pol];
double *p;
int *Xp,*Yp, *Zp;
idx=(int *) malloc ((size_t)(pol*pol*pol)*sizeof(int));
g_indx=(int *) malloc ((size_t)(pol*pol*pol)*sizeof(int));
Xp=(int *)malloc(Npos*sizeof(int));    
Yp=(int *)malloc(Npos*sizeof(int));    
Zp=(int *)malloc(Npos*sizeof(int));    

char name[80];
char folder[]="./data3d/";
strcpy(name,folder);
strcat(name,filename);

FILE *fpa;
if ((fpa=fopen(name,"w"))==NULL) { printf("cannot open %s\n",filename); exit(0);}

//for each position
for (i=0; i<Npos; i++) {

	interp_stencil(pol, xp[i], yp[i], zp[i],X_interp, Y_interp, Z_interp, x_interp, y_interp, z_interp);
	
	//will get indices in computational set  of the points in the interpolation stencil		
	for (iz=0; iz<pol; iz++) {
		for (ix=0; ix<pol; ix++) {
			for (iy=0; iy<pol; iy++) {
				//index in interp box
				tmp=iz*pol*pol+ix*pol+iy;
				//Global index of each of the points in the interpolation stencil          
				g_indx[tmp]=X_interp[ix]*m2+Y_interp[iy]+Z_interp[iz]*m2*m1;
				//index in computational set of each of the points in the interp stencil
				idx[tmp]=indx_in_comp_set[g_indx[tmp]];
				if(idx[tmp]<0){printf("idx < 0 in %s\n",filename);
				printf("glob indx, index in set %d %d\n",g_indx[tmp], i);
				exit(0);
				}
			}
		}
	}//end iz
	//vector of weights of each od the point in the interp stencil
        p=weight3d(x_interp,y_interp,z_interp,xp[i],yp[i],zp[i],pol);
        for (k=0; k<pol*pol*pol; k++) {
            //IPM[i][idx[k]]=p[k];           
            fprintf(fpa,"%d,%d,%.12e\n",i+1,idx[k]+1,p[k]);
        }

}//end for i

//To get the desired size of the matrix in Matlab
fprintf(fpa,"%d,%d,%e\n",Npos,Ncs,0.);

free(idx);
free(g_indx);
fclose(fpa);
free(Xp);
free(Yp);
free(Zp);
printf("%s completed\n",filename);
}

void Get_E(int N,int Ng, int **Center,int **Cen_g, double x[],double y[], double z[], double xc[],double yc[],double zc[], double xc_g[],double yc_g[],double zc_g[],int indx_in_U[]){
    int *idx,i,j,ix,iy,iz,k,tmp;
    int X_interp[pol],Y_interp[pol],Z_interp[pol];
    double x_interp[pol],y_interp[pol],z_interp[pol];
    double *p;
    idx=(int *) malloc ((size_t)(pol*pol*pol)*sizeof(int));
    FILE *fp;
    if ((fp=fopen("./data3d/E_matrix","w"))==NULL) {
        printf("cannot open print file\n");
        exit(0);
    }

    //for band points
    for (i=0; i<N; i++) {

        interp_stencil(pol,xc[i],yc[i],zc[i], X_interp, Y_interp, Z_interp, x_interp, y_interp, z_interp );
        for (iz=0; iz<pol; iz++) {
            for (ix=0; ix<pol; ix++) {
                for (iy=0; iy<pol; iy++) {
                    tmp=iz*pol*pol+ix*pol+iy;
                    idx[tmp]=indx_in_U[X_interp[ix]*m2+Y_interp[iy]+Z_interp[iz]*m1*m2];
					if(idx[tmp]<0){printf("In Get_E, Point in stencil not in U: %d\n", tmp);exit(0);}			
        	    }
            }
        }//end iz

        p=weight3d(x_interp,y_interp,z_interp,xc[i],yc[i],zc[i],pol);
        for (k=0; k<pol*pol*pol; k++) {
          //  E[i][idx[k]]=p[k];
	if(p[k]!=p[k]){printf("In Get_E, NaN weight\n");exit(0);}
            fprintf(fp,"%d,%d,%.12e\n",i+1,idx[k]+1,p[k]);
        }
    }//end for i

    printf("completed interpolation for band points\n");
    //for ghost points
    for (i=0; i<Ng; i++) {

        interp_stencil(pol,xc_g[i],yc_g[i],zc_g[i], X_interp, Y_interp, Z_interp, x_interp, y_interp, z_interp );

        for (iz=0; iz<pol; iz++) {
            for (ix=0; ix<pol; ix++) {
                for (iy=0; iy<pol; iy++) {
                    tmp=iz*pol*pol+ix*pol+iy;
                    idx[tmp]=indx_in_U[X_interp[ix]*m2+Y_interp[iy]+Z_interp[iz]*m1*m2];
                }
            }
        }

        p=weight3d(x_interp,y_interp,z_interp,xc_g[i],yc_g[i],zc_g[i],pol);
        for (k=0; k<pol*pol*pol; k++) {
           // E[i+N][idx[k]]=p[k];
            fprintf(fp,"%d,%d,%.12e\n",i+N+1,idx[k]+1,p[k]);
        }
    }//end for i 
    printf("completed interpolation for ghost points\n");
    fprintf(fp,"%d,%d,%.12e\n",Ng+N,N,0.);

    free(idx);
    fclose(fp);
}

void Get_Delta_h(int N, int indx_in_U[], int indx_ghost[], int ** map, int U_evolve_new[]){

    int Xind[6]={0,0,-1,0,0,1},Yind[6]={0,-1,0,0,1,0},Zind[6]={-1,0,0,1,0,0};
    int X_interp[6],Y_interp[6],Z_interp[6];
    int ix,iy,iz,i,j,k,tmp,temp;
    int gh_tmp;
    FILE *fp;
    if ((fp=fopen("./data3d/Delta_h","w"))==NULL) {
                printf("cannot open print file\n");
                exit(0);
        }
//The diagonal element is not included in the matrix. It's added after multiplying by E (in the Matlab script) due to stability issues
    for (j=0; j<N; j++) {
        tmp=U_evolve_new[j];
        ix=map[tmp][0];
        iy=map[tmp][1];
        iz=map[tmp][2];
        for (k=0; k<6; k++) {
            X_interp[k]=Xind[k]+ix;
            Y_interp[k]=Yind[k]+iy;
            Z_interp[k]=Zind[k]+iz;
            temp=indx_in_U[X_interp[k]*m2+Y_interp[k]+Z_interp[k]*m1*m2];
            if (temp<0) {
                gh_tmp=indx_ghost[X_interp[k]*m2+Y_interp[k]+Z_interp[k]*m1*m2];
                //Jian-geng modified: changed "Ghost point not found in Delta_h" to report the j in U_evolve_new;
                if(gh_tmp<0){printf("%d\n", j);}
                //Dh[j][N+gh_tmp]=1;
                fprintf(fp,"%d,%d,%e\n",j+1,N+gh_tmp+1,1.);
            }//end if
            else {
              //  Dh[j][temp]=1;
                fprintf(fp,"%d,%d,%e\n",j+1,temp+1,1.);
            }
        }//end k
    }
    fclose(fp);
}

void Get_E_vext_band(int Nv_ext,int **Cen_Vext,  double xc_Vext[],double yc_Vext[],double zc_Vext[],int indx_into_U[],int N){

    int *idx,*g_indx,i,j,ix,iy,iz,k,tmp;
    int X_interp[pol],Y_interp[pol],Z_interp[pol];
    double x_interp[pol],y_interp[pol],z_interp[pol];
    double *p;
    idx=(int *) malloc ((size_t)(pol*pol*pol)*sizeof(int));
    g_indx=(int *) malloc ((size_t)(pol*pol*pol)*sizeof(int));


    FILE *fpv;
    if ((fpv=fopen("./data3d/E_vext_band","w"))==NULL) {
        printf("cannot open E_vext_band file\n");
        exit(0);
    }
   
    //for V_ext
    for (i=0; i<Nv_ext; i++) {

        interp_stencil(pol, xc_Vext[i], yc_Vext[i], zc_Vext[i], X_interp, Y_interp, Z_interp, x_interp, y_interp, z_interp);

        for (iz=0; iz<pol; iz++) {
            for (ix=0; ix<pol; ix++) {
                for (iy=0; iy<pol; iy++) {
                                        //index in interp stencil
                    tmp=iz*pol*pol+ix*pol+iy;
                                        //Global index of each of the points in the interpolation stencil                                       
                                        g_indx[tmp]=X_interp[ix]*m2+Y_interp[iy]+Z_interp[iz]*m1*m2;
                                        //index in U comp band of each of the points in the interp stencil
                                        idx[tmp]=indx_into_U[g_indx[tmp]];
                }
            }
        }//end iz

        p=weight3d(x_interp,y_interp,z_interp,xc_Vext[i],yc_Vext[i],zc_Vext[i],pol);
        for (k=0; k<pol*pol*pol; k++) {
            //E_vext_band[i][idx[k]]=p[k];           
            fprintf(fpv,"%d,%d,%.12e\n",i+1,idx[k]+1,p[k]);
        }

    }//end for i

        //To get the desired size of the matrix in Matlab
    fprintf(fpv,"%d,%d,%e\n",Nv_ext,N,0.);


    free(idx);
    free(g_indx);
    fclose(fpv);


        printf("E_vext_band completed\n");
}


void Get_E_set_vint(char filename[],int Nset,int **Cen_set, double x[],double y[], double z[], double xcp_set[],double ycp_set[],double zcp_set[],int indx_in_Vint[],int Nv_int, double ** cp_data, int * inside){

int *idx,*g_indx,i,j,ix,iy,iz,k,tmp,countout,gl_temp;
int X_extrap[pol],Y_extrap[pol],Z_extrap[pol];
double x_extrap[pol],y_extrap[pol],z_extrap[pol];
double *p;
double *xcpVer, *ycpVer, *zcpVer;
int **Ver;
int Step_X,Step_Y,Step_Z, dispX, dispY, dispZ;

xcpVer=(double *)malloc(Nset*sizeof(double));
ycpVer=(double *)malloc(Nset*sizeof(double));
zcpVer=(double *)malloc(Nset*sizeof(double));

Ver=(int **)malloc(Nset*sizeof(int *));
for (i=0; i<Nset; i++) {
        Ver[i]=(int *)malloc(3*sizeof(int));
        }

idx=(int *) malloc ((size_t)(pol*pol*pol)*sizeof(int));
g_indx=(int *) malloc ((size_t)(pol*pol*pol)*sizeof(int));

char name[80];
char folder[]="./data3d/";
strcpy(name,folder);
strcat(name,filename);
int out;
FILE *fpv;
if ((fpv=fopen(name,"w"))==NULL) {printf("cannot open %s\n",filename);exit(0);}

vertex_stencil(Cen_set, Nset, x, y, z, xcp_set, ycp_set, zcp_set, Ver,inside);
//for set

for (i=0; i<Nset; i++) {
        //closest point of Ver
	
	//gets global index of Ver from integer coordinates:
	gl_temp=Ver[i][2]*m2*m1+Ver[i][0]*m2+Ver[i][1];	

        xcpVer[i]=cp_data[gl_temp][0];
        ycpVer[i]=cp_data[gl_temp][1];
        zcpVer[i]=cp_data[gl_temp][2];

        //Extrapolation stencil for cp[i]

        //Aiming that the extrapolation stencil is inside v, the coordinates are calculated from the vertex towards the inside

        if(x[Ver[i][0]]-xcpVer[i]>=0){Step_X=1;}
        else{Step_X=-1;}

        if(y[Ver[i][1]]-ycpVer[i]>=0){Step_Y=1;}
        else{Step_Y=-1;}

        if(z[Ver[i][2]]-zcpVer[i]>=0){Step_Z=1;}
        else{Step_Z=-1;}

///try different displacements from vertex trying to fit the stencil inside V
	for(dispX=0; dispX<=disp; dispX++){
	for(dispY=0; dispY<=disp; dispY++){
	for(dispZ=0; dispZ<=disp; dispZ++){

		for(j=0;j<pol;j++){
	
        	        X_extrap[j]=Ver[i][0]+Step_X*(j+dispX);
			Y_extrap[j]=Ver[i][1]+Step_Y*(j+dispY);
			Z_extrap[j]=Ver[i][2]+Step_Z*(j+dispZ);

 //get real coordinates of the extrapolation stencil
			x_extrap[j]=x[X_extrap[j]];
			y_extrap[j]=y[Y_extrap[j]];
			z_extrap[j]=z[Z_extrap[j]];
		}

        //Check if there are points in the stencil out of v
        	out=0;
		for (iz=0; iz<pol; iz++) {
		for (ix=0; ix<pol; ix++) {
		for (iy=0; iy<pol; iy++) {

			gl_temp=Z_extrap[iz]*m1*m2 + X_extrap[ix]*m2 + Y_extrap[iy];
			if(inside[gl_temp]==0){
			out=1;
			}
        	}//iy
        	}//ix
        	}//iz
	
		if(out==0){break;}

	}//dispZ
	
		if(out==0){break;}
	}//dispY
		if(out==0){break;}
	}//dispX 

	for (iz=0; iz<pol; iz++) {
        for (ix=0; ix<pol; ix++) {
        for (iy=0; iy<pol; iy++) {

		gl_temp=Z_extrap[iz]*m1*m2 + X_extrap[ix]*m2 + Y_extrap[iy];
                if(inside[gl_temp]==0){
        //Jian-geng modified to print error positions cleanly
		//printf("i dx ix iy iz  %d %f %d %d %d\n",i ,dx ,ix, iy, iz);	
		//printf("vert r x y z %f %f %f %f\n",sqrt(pow(x[Ver[i][0]],2)+pow(y[Ver[i][1]],2)),x[Ver[i][0]],y[Ver[i][1]],z[Ver[i][2]]);
		//printf("extr r x y z %f %f %f %f\n",sqrt(pow(x_extrap[ix],2)+pow(y_extrap[iy],2)),x_extrap[ix],y_extrap[iy],z_extrap[iz]);	
		//printf("steps x y z %d %d %d\n", Step_X, Step_Y, Step_Z);
		//printf("cp ver x y z %f %f %f\n",xcpVer[i], ycpVer[i], zcpVer[i]);
		//exit(0);
        printf("%d\n", i);
        break;
                }
        }//iy
        }//ix
        }//iz

        //Get matrix values        
        for (iz=0; iz<pol; iz++) {
                for (ix=0; ix<pol; ix++) {
                        for (iy=0; iy<pol; iy++) {
                        //index in interp stencil
                        tmp=iz*pol*pol+ix*pol+iy;
                        //Global index of each of the points in the extrapolation stencil                                      
                        g_indx[tmp]=X_extrap[ix]*m2+Y_extrap[iy]+Z_extrap[iz]*m1*m2;
                        //index in V_int of each of the points in the extrapolation stencil
                        idx[tmp]=indx_in_Vint[g_indx[tmp]];
                        		//Jian-geng modified so the code runs
                                //if(idx[tmp]<0){printf("A point for the extrapolation stencil was not in V_int, i=%d\n",i);
                                //printf("%d %d %d\n",X_extrap[ix],Y_extrap[iy],Z_extrap[iz]);
                                //exit(0);
                                //}
                        }
                }
        }//end iz

        p=weight3d(x_extrap,y_extrap,z_extrap,xcp_set[i],ycp_set[i],zcp_set[i],pol);
        for (k=0; k<pol*pol*pol; k++) {
                //E_set_vint[i][idx[k]]=p[k];           
                fprintf(fpv,"%d,%d,%.12e\n",i+1,idx[k]+1,p[k]);
        }

}//end for i

        //To get the desired size of the matrix in Matlab
        fprintf(fpv,"%d,%d,%e\n",Nset,Nv_int,0.);

free(xcpVer);
free(ycpVer);
free(zcpVer);

for (i=0; i<Nset; i++) {free(Ver[i]);}
free(Ver);

free(idx);
free(g_indx);
fclose(fpv);


        printf("%s completed\n",filename);
}

void Get_Mi_vext_vint(int Nv_ext,int **Cen_Vext, double x[],double y[], double z[], double xcp_Vext[],double ycp_Vext[],double zcp_Vext[],int indx_in_Vint[],int Nv_int, int V_ext[],int **map,double ** cp_data, int * inside){

int *idx,*g_indx,i,j,ix,iy,iz,k,tmp, out, countout, gl_temp;
idx=(int *) malloc ((size_t)(pol*pol*pol)*sizeof(int));
g_indx=(int *) malloc ((size_t)(pol*pol*pol)*sizeof(int));

double *xmi_Vext, *ymi_Vext, *zmi_Vext;
int ** Mi_Vext;

xmi_Vext=(double *)malloc(Nv_ext*sizeof(double));
ymi_Vext=(double *)malloc(Nv_ext*sizeof(double));
zmi_Vext=(double *)malloc(Nv_ext*sizeof(double));
Mi_Vext=(int **)malloc(Nv_ext*sizeof(int *));
for(i=0;i<Nv_ext;i++){Mi_Vext[i]=(int *)malloc(3*sizeof(int));}

int X_extrap[pol],Y_extrap[pol],Z_extrap[pol];
double x_extrap[pol],y_extrap[pol],z_extrap[pol];
double *p;
double *xcpVer, *ycpVer, *zcpVer;
int **Ver;
int Step_X,Step_Y,Step_Z, dispX, dispY, dispZ;

xcpVer=(double *)malloc(Nv_ext*sizeof(double));
ycpVer=(double *)malloc(Nv_ext*sizeof(double));
zcpVer=(double *)malloc(Nv_ext*sizeof(double));

Ver=(int **)malloc(Nv_ext*sizeof(int *));
for (i=0; i<Nv_ext; i++) {Ver[i]=(int *)malloc(3*sizeof(int));}

    FILE *fpv;
    if ((fpv=fopen("./data3d/Mi_vext_vint","w"))==NULL) {
        printf("cannot open Mi_vext_vint file\n");
        exit(0);
    }

//for V_ext get mirror points 

for (i=0; i<Nv_ext; i++) {

        //get mirror point real coordinates:  m=cp+(cp-x)
        //map has integer coordinates that are used in x,y,z
        xmi_Vext[i]=2*xcp_Vext[i]-x[map[V_ext[i]][0]];
        ymi_Vext[i]=2*ycp_Vext[i]-y[map[V_ext[i]][1]];
        zmi_Vext[i]=2*zcp_Vext[i]-z[map[V_ext[i]][2]];
        //get integer coordinate of mirror point

        Mi_Vext[i][0]=lrint((xmi_Vext[i]+L1)/dx);
        Mi_Vext[i][1]=lrint((ymi_Vext[i]+L2)/dx);
        Mi_Vext[i][2]=lrint((zmi_Vext[i]+L3)/dx);
}

//Get vertex for the intrapolation stencil inside v
//Use Mi instead of Cen
//Select the point around Mi that is closer to cp to be the vertex

vertex_stencil(Mi_Vext, Nv_ext, x, y, z, xcp_Vext, ycp_Vext, zcp_Vext, Ver, inside);
//for V_ext
countout=0;
for (i=0; i<Nv_ext; i++) {
        //closest point of Ver
	
	//gets global index of Ver from integer coordinates:
	gl_temp=Ver[i][2]*m2*m1+Ver[i][0]*m2+Ver[i][1];	

        xcpVer[i]=cp_data[gl_temp][0];
        ycpVer[i]=cp_data[gl_temp][1];
        zcpVer[i]=cp_data[gl_temp][2];

        //Extrapolation stencil for cp[i]

        //Aiming that the extrapolation stencil is inside v, the coordinates are calculated from the vertex towards the inside

        if(x[Ver[i][0]]-xcpVer[i]>=0){Step_X=1;}
        else{Step_X=-1;}

        if(y[Ver[i][1]]-ycpVer[i]>=0){Step_Y=1;}
        else{Step_Y=-1;}

        if(z[Ver[i][2]]-zcpVer[i]>=0){Step_Z=1;}
        else{Step_Z=-1;}

///try different displacements from vertex trying to fit the stencil inside V
for(dispX=0; dispX<=disp; dispX++){
for(dispY=0; dispY<=disp; dispY++){
for(dispZ=0; dispZ<=disp; dispZ++){

	for(j=0;j<pol;j++){

                X_extrap[j]=Ver[i][0]+Step_X*(j+dispX);
                Y_extrap[j]=Ver[i][1]+Step_Y*(j+dispY);
                Z_extrap[j]=Ver[i][2]+Step_Z*(j+dispZ);

 //get real coordinates of the extrapolation stencil
                x_extrap[j]=x[X_extrap[j]];
                y_extrap[j]=y[Y_extrap[j]];
                z_extrap[j]=z[Z_extrap[j]];
	}

        //Check if there are points in the stencil out of v
        out=0;
        for (iz=0; iz<pol; iz++) {
        for (ix=0; ix<pol; ix++) {
        for (iy=0; iy<pol; iy++) {

		gl_temp=Z_extrap[iz]*m1*m2 + X_extrap[ix]*m2 + Y_extrap[iy];
                if(inside[gl_temp]==0){
                out=1;
                }
        }//iy
        }//ix
        }//iz
	
	if(out==0){break;}

}//dispZ
	if(out==0){break;}
}//dispY
	if(out==0){break;}
}//dispX 

// Testing that the stencil is inside

	for (iz=0; iz<pol; iz++) {
        for (ix=0; ix<pol; ix++) {
        for (iy=0; iy<pol; iy++) {

		gl_temp=Z_extrap[iz]*m1*m2 + X_extrap[ix]*m2 + Y_extrap[iy];
                if(inside[gl_temp]==0){
		printf("i dx ix iy iz  %d %f %d %d %d\n",i ,dx ,ix, iy, iz);	
		printf("vert r x y z %f %f %f %f\n",sqrt(pow(x[Ver[i][0]],2)+pow(y[Ver[i][1]],2)),x[Ver[i][0]],y[Ver[i][1]],z[Ver[i][2]]);
		printf("extr r x y z %f %f %f %f\n",sqrt(pow(x_extrap[ix],2)+pow(y_extrap[iy],2)),x_extrap[ix],y_extrap[iy],z_extrap[iz]);	
		printf("steps x y z %d %d %d\n", Step_X, Step_Y, Step_Z);
		printf("cp ver x y z %f %f %f\n",xcpVer[i], ycpVer[i], zcpVer[i]);
		exit(0);
                }
        }//iy
        }//ix
        }//iz

        //Get matrix values        
        for (iz=0; iz<pol; iz++) {
                for (ix=0; ix<pol; ix++) {
                        for (iy=0; iy<pol; iy++) {
                        //index in interp stencil
                        tmp=iz*pol*pol+ix*pol+iy;
                        //Global index of each of the points in the extrapolation stencil                                       
                        g_indx[tmp]=X_extrap[ix]*m2+Y_extrap[iy]+Z_extrap[iz]*m1*m2;
                        //index in V_int of each of the points in the extrapolation stencil
                        idx[tmp]=indx_in_Vint[g_indx[tmp]];
                        if(idx[tmp]<0){printf("A point for the Mi extrapolation stencil was not in V_int\n");}
                        }
                }
        }//end iz

        p=weight3d(x_extrap,y_extrap,z_extrap,xmi_Vext[i],ymi_Vext[i],zmi_Vext[i],pol);
        for (k=0; k<pol*pol*pol; k++) {
                //Mi_ext_v[i][idx[k]]=p[k];           
                fprintf(fpv,"%d,%d,%.12e\n",i+1,idx[k]+1,p[k]);
        }
        //To get the desired size of the matrix in Matlab
    fprintf(fpv,"%d,%d,%e\n",Nv_ext,Nv_int,0.);


 }//for Nv_ext

    printf("Number of corrected stencils %d out of %d\n",countout,Nv_ext);
free(xcpVer);
free(ycpVer);
free(zcpVer);

for (i=0; i<Nv_ext; i++) {free(Ver[i]);}
free(Ver);

free(xmi_Vext);
free(ymi_Vext);
free(zmi_Vext);
for(i=0;i<Nv_ext;i++){free(Mi_Vext[i]);}
free(Mi_Vext);
free(idx);
free(g_indx);
fclose(fpv);

printf("Mi_vext_vint completed\n");
}


void Get_Delta_h_v(int Nv_tot, int Nv_int, int N_ext, int indx_into_V_tot[], int ** map, int V_tot[]){

int Xind[6]={0,0,-1,0,0,1},Yind[6]={0,-1,0,0,1,0},Zind[6]={-1,0,0,1,0,0};
int X_interp[6],Y_interp[6],Z_interp[6];
int ix,iy,iz,i,j,k,tmp,temp;
int gh_tmp;

FILE *fp;
if ((fp=fopen("./data3d/Delta_h_v","w"))==NULL) {
printf("cannot open print file\n");
exit(0);
}

//for all points in Vint
for (j=0; j<Nv_int; j++) {
        //-6 in the diagonal
        fprintf(fp,"%d,%d,%e\n",j+1,j+1,-6.);
        tmp=V_tot[j];
        //get integer coordinates of the point
        ix=map[tmp][0];
        iy=map[tmp][1];
        iz=map[tmp][2];
        //get integer coordinates of diff stencil of the point 
        for (k=0; k<6; k++) {
                X_interp[k]=Xind[k]+ix;
                Y_interp[k]=Yind[k]+iy;
                Z_interp[k]=Zind[k]+iz;
                //get index in V_tot of each point in diff stencil
                temp=indx_into_V_tot[X_interp[k]*m2+Y_interp[k]+Z_interp[k]*m1*m2];
                //Dh[j][temp]=1;
                if(temp==-1){printf("negative index in Get_Delta_h_v\n");exit(0);}
                fprintf(fp,"%d,%d,%e\n",j+1,temp+1,1.);
        }//end k
//The size of the matrix is Nv_int x Nv_tot
fprintf(fp,"%d,%d,%e\n",Nv_int,Nv_tot,0.);

}//end j
fclose(fp);

printf("V Laplacian completed\n");
}//End function

double *weight3d(double Xs[],double Ys[],double Zs[],double xc,double yc,double zc,int len_xs){
    int i,j,tmp,ix,iy,iz;
    double *wx,*wy,*wz,*Hx,*Hy,*Hz;
    static double *p;
    double sumHx=0,sumHy=0,sumHz=0;
    int nn=len_xs*len_xs*len_xs;
    wx=(double *) malloc((size_t)(len_xs)*sizeof(double));
    wy=(double *) malloc((size_t)(len_xs)*sizeof(double));
    wz=(double *) malloc((size_t)(len_xs)*sizeof(double));
    Hx=(double *) malloc((size_t)(len_xs)*sizeof(double));
    Hy=(double *) malloc((size_t)(len_xs)*sizeof(double));
    Hz=(double *) malloc((size_t)(len_xs)*sizeof(double));
    p=(double *) malloc((size_t)(nn)*sizeof(double));
   
   
    for (i=0; i<len_xs; i++) {
        wx[i]=1;
        wy[i]=1;
        wz[i]=1;
    }
    for (i=0; i<len_xs; i++) {
        for (j=0; j<len_xs; j++) {
            if (j==i) {
                continue;
            }
            else {
                wx[i]*=Xs[i]-Xs[j];
                wy[i]*=Ys[i]-Ys[j];
                wz[i]*=Zs[i]-Zs[j];
            }

        }//end for
    }//end for

    for (i=0; i<len_xs; i++) {
	if(wx[i]==0){printf("wx[i]==0\n");exit(0);}
	if(wy[i]==0){printf("wy[i]==0\n");exit(0);}
	if(wz[i]==0){printf("wz[i]==0\n");exit(0);}
        wx[i]=1/wx[i];
        wy[i]=1/wy[i];
        wz[i]=1/wz[i];
	if(wx[i]!=wx[i]){printf("wx[i] NaN\n");}	
	if(wy[i]!=wy[i]){printf("wy[i] NaN\n");}	
	if(wz[i]!=wz[i]){printf("wz[i] NaN\n");}	
    }

// if xc  is on a grid point, the weight of that grid point must be one, and the weight of the other grid points must be zero
// The same applies for yc and zc
//The variables ix, iy, iz, are used here to track if that happens 

	ix=-1; iy=-1; iz=-1;
    for (i=0; i<len_xs; i++) {

	if(xc==Xs[i]){ix=i;printf("xc==Xs\n");}
	if(yc==Ys[i]){iy=i;printf("yc==Ys\n");} 
	if(zc==Zs[i]){iz=i;printf("zc==Zs\n");}
		
        Hx[i]=wx[i]/(xc-Xs[i]);
        Hy[i]=wy[i]/(yc-Ys[i]);
        Hz[i]=wz[i]/(zc-Zs[i]);

	if(Hx[i]!=Hx[i]){printf("Hx[i] NaN %f %f %f\n",wx[i],xc,Xs[i]);}	
	if(Hy[i]!=Hy[i]){printf("Hy[i] NaN %d\n",i);}	
	if(Hz[i]!=Hz[i]){printf("Hz[i] NaN %d\n",i);}	
        sumHx+=Hx[i];
        sumHy+=Hy[i];
        sumHz+=Hz[i];
    }
	if(ix > -1){
		sumHx=1;
		for(i=0;i<len_xs;i++){
		if(i==ix){Hx[i]=1;}
		else{Hx[i]=0;}
		}
	}
	if(iy > -1){
		sumHy=1;
		for(i=0;i<len_xs;i++){
		if(i==iy){Hy[i]=1;}
		else{Hy[i]=0;}
		}
	}
	if(iz > -1){
		sumHz=1;
		for(i=0;i<len_xs;i++){
		if(i==iz){Hz[i]=1;}
		else{Hz[i]=0;}
		}
	}

	
	if(sumHx!=sumHx){printf("sumHx==NaN\n"),exit(0);}
	if(sumHy!=sumHy){printf("sumHy==NaN\n"),exit(0);}
	if(sumHz!=sumHz){printf("sumHz==NaN\n"),exit(0);}

	if(sumHx==0){printf("sumHx==0\n"),exit(0);}
	if(sumHy==0){printf("sumHy==0\n"),exit(0);}
	if(sumHz==0){printf("sumHz==0\n"),exit(0);}
    for (iz=0; iz<len_xs; iz++) {
        for (ix=0; ix<len_xs; ix++) {
            for (iy=0; iy<len_xs; iy++) {
                tmp=iz*len_xs*len_xs+ix*len_xs+iy;
                p[tmp]=Hx[ix]*Hy[iy]*Hz[iz]/(sumHx*sumHy*sumHz);
		if(p[tmp]!=p[tmp]){
		printf("In weight function %f %f %f %f %f %f %d\n",Hx[ix],Hy[iy],Hz[iz],sumHx,sumHy,sumHz,iy);exit(0);
		}
            }
        }
    }//end for iz
    free(wx);
    free(wy);
    free(wz);
    free(Hx);
    free(Hy);
    free(Hz);

    return p;
}

//Jian-geng modified to change shape
//Eight radius  
double db_rad(double z, void * params){

db_params * p = (db_params *) params;
double a= p->a;
double b= p->b; 
double c= p->c;
double d= p->d;
double e= p->e;
double f= p->f;

if (z<f){
	return sqrt(a*a-(z+d)*(z+d));
}
else{
	return sqrt(c*c-c*c*(z+e)*(z+e)/(b*b));
}
}

//Distance from a point on the eight z,r to the point zpt rpt defined in params 
double db_dist(double z, void * params){

struct db_params * p = (struct db_params *) params;
double rpt= p->rpt;
double zpt= p->zpt;
double r= db_rad(z,p);

return sqrt(pow(r-rpt ,2) + pow(z-zpt,2));
}

//Jian-geng modified to change shape
//Squared distance derivative
double db_sq_dist_deriv(double z, void * params){

struct db_params * p = (struct db_params *) params;
double a= p->a;
double b= p->b; 
double c= p->c;
double d= p->d;
double e= p->e;
double f= p->f;
double rpt= p->rpt;
double zpt= p->zpt;
double r= db_rad(z,p);

if (z<f){
	return (-2*z-2*d)*(r-rpt)/r + 2*(z-zpt);
}
else{
	return (-2*c*c/(b*b))*(z+e)*(r-rpt)/r + 2*(z-zpt);
}
}



